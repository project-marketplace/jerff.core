<?

namespace Jerff\Core\Agent;

use Exception;
use Jerff\Core\Model\Error;

class Agent
{

    /**
     * @return string
     * @throws \Exception
     */
    static public function process($page = 1)
    {
        try {
            $page = self::update($page);
        } catch (Exception $e) {
            Error::send($e);
        }
        return '\Jerff\Core\Agent\Action::process(' . $page . ');';
    }

    /**
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\LoaderException
     */
    public static function update($page)
    {
        return $page;
    }

}
