<?php

namespace Jerff\Core\Model;

use Exception;
use Jerff\Core\Database\ErrorTable;

class Error
{

    /**
     * @param Exception|AmoCRMApiException $e
     *
     * @throws Exception
     */
    static public function view(Exception $e)
    {
        pre(self::get($e));
    }

    /**
     * @param Exception $e
     *
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    static public function sendException(Exception $e = null)
    {
        self::mail($e->getMessage(), self::get($e));
    }

    /**
     * @param string    $type
     * @param array     $arData
     * @param Exception $e
     *
     * @throws Exception
     */
    static public function send(string $type, array $arData = [], Exception $e = null)
    {
        $arSend = [
            $arData,
        ];
        if (empty($e)) {
            $e = new Exception();
        }
        $arSend[$type] = self::get($e);
        self::mail($type, $arSend);
        exit;
    }

    static protected function get(Exception $e): array
    {
        $arSend = [
            $e->getLine(),
            $e->getFile(),
            $e->getCode(),
            $e->getMessage(),
            $e->getTraceAsString(),
        ];
        if($e instanceof \Bitrix\Main\DB\SqlQueryException) {
            $arSend[] = $e->getQuery();
        }
        return $arSend;
    }

    /**
     * @param string $type
     * @param array  $arSend
     *
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    static protected function mail(string $type, array $arSend)
    {
        pre($type, $arSend);
        ErrorTable::add([
            'TYPE' => $type,
            'DATE' => date('d.m.Y H:i:s'),
            'DATA' => serialize($arSend),
        ]);
    }

}
