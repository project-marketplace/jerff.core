<?php

namespace Jerff\Core\Database;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;

class ErrorTable extends DataManager
{

    public static function getTableName()
    {
        return 'jerff_core_error';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'primary'      => true,
                'autocomplete' => true,
            ]),
            new Entity\StringField('DATE'),
            new Entity\StringField('TYPE'),
            new Entity\TextField('DATA'),
        ];
    }

}