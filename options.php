<?
if (!$USER->IsAdmin())
    return;

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Config\Option;

$moduleId = 'jerff.core';
Loader::includeModule($moduleId);

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('JERFF_CORE_TAB_CONFIG'),
        'OPTIONS' => array(
            array("adminDebug", Loc::getMessage("JERFF_CORE_ADMIN_DEBUG"), "1", array("text", 80)),
        )
    ),
    [
        'DIV' => 'edit2',
        'TAB' => Loc::getMessage('JERFF_CORE_TAB_ACCESS'),
    ]
);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && check_bitrix_sessid()) {
    if (!empty($_REQUEST['save'])) {
        foreach ($aTabs as $aTab) {
            __AdmSettingsSaveOptions($moduleId, $aTab['OPTIONS']);
        }
    }
    if (!empty($_REQUEST['RestoreDefaults'])) {
        Option::delete($moduleId);
        foreach ($aTabs as $aTab) {
            foreach ($aTab['OPTIONS'] as $arOption) {
                if ($arOption[2]) {
                    Option::set($moduleId, $arOption[0], $arOption[2]);
                }
            }
        }
    }

    ob_start();
    $Update = $Update.$Apply;
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights2.php");
    ob_end_clean();

    LocalRedirect($APPLICATION->GetCurPage() . '?lang=' . LANGUAGE_ID . '&mid_menu=1&mid=' . urlencode($moduleId) .
        '&tabControl_active_tab=' . urlencode($_REQUEST['tabControl_active_tab']) . '&sid=' . urlencode($siteId));
}

$tabControl = new CAdminTabControl('tabControl', $aTabs);
?>
    <form method='post' action='' name='bootstrap'>
        <?
        $tabControl->Begin();

        foreach ($aTabs as $aTab) {
            $tabControl->BeginNextTab();
            __AdmSettingsDrawList($moduleId, $aTab['OPTIONS']);
        }

        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights2.php");

        $tabControl->Buttons(array('btnApply' => false, 'btnCancel' => false, 'btnSaveAndAdd' => false));
        ?>
        <input type="submit" name="RestoreDefaults" title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS") ?>"
               OnClick="confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING")) ?>')"
               value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS") ?>">
        <?= bitrix_sessid_post(); ?>
        <? $tabControl->End(); ?>
    </form>
<?