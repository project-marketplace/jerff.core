<?

if (IsModuleInstalled('jerff.core')) {
    if (is_dir(__DIR__ . '/install/components'))
        $updater->CopyFiles("install/components", "components/");
    Project\Tools\Modules\Utility::createDbTable('\Jerff\Core\Database\ErrorTable');
    foreach (array(
                 [
                     'name' => '\Jerff\Core\Agent\Agent::process();',
                     'period' => 'Y',
                     'interval' => 60 * 60 * 24,
                 ]
             ) as $agent) {
        Project\Tools\Utility\Agent::add(
            $agent['name'], 'jerff.core', $agent['period'], $agent['interval']
        );
    }
}
