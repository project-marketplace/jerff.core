<?php

if (!defined('\Project\Tools\Modules\IS_START')) {
    include_once(dirname(__DIR__) . '/project.tools/include.php');
}

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Project\Tools\Modules;

Loc::loadMessages(__FILE__);

class jerff_core extends CModule
{

    public $MODULE_ID = 'jerff.core';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $SHOW_SUPER_ADMIN_GROUP_RIGHTS = "Y";
    public $MODULE_GROUP_RIGHTS = "Y";
    const MODULE_CODE = 'jerff';
    const PARTNER_CODE = '';
    const MIN_MAIN_VERSION = '16.0.0';
    const REQUIRE_MODULES = ['main', 'iblock'];

    use Modules\Install;

    const TABLE = [
        '\Jerff\Core\Database\ErrorTable',
    ];
    const EVENT = [
        [
            'modul' => 'main',
            'event' => 'OnPageStart',
            'class' => '\Jerff\Core\Event\Page',
            'func' => 'OnPageStart',
        ],
    ];
    const AGENT = [
        [
            'name' => '\Jerff\Core\Agent\Agent::process();',
            'period' => 'Y',
            'interval' => 60 * 60 * 24,
        ],
    ];
    const FILES = [
        '/components/'=>'/bitrix/components/',
    ];

    function __construct()
    {
        $this->setParam(__DIR__, 'JERFF_CORE');
        $this->MODULE_NAME = Loc::getMessage('JERFF_CORE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('JERFF_CORE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('JERFF_CORE_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('JERFF_CORE_PARTNER_URI');
    }

    function GetModuleRightList()
    {
        $arr = array(
            "reference_id" => array("D", "U"),
            "reference" => array(
                "[D] ".Loc::getMessage("JERFF_CORE_SINS_PERM_D"),
                "[U] ".Loc::getMessage("JERFF_CORE_SINS_PERM_U"),
            )
        );
        return $arr;
    }

    public function DoInstall()
    {
        if (self::REQUIRE_MODULES) {
            foreach (self::REQUIRE_MODULES as $module) {
                if (!IsModuleInstalled($module)) {
                    global $APPLICATION;
                    $APPLICATION->ThrowException($this->getMessage('INSTALL_REQUIRE_MODULES_ERROR', ['#MODULE#' => $module]));
                }
            }
        }

        if (strlen(self::MIN_MAIN_VERSION) <= 0 || version_compare(SM_VERSION, self::MIN_MAIN_VERSION) >= 0) {
            $this->Install();
        } else {
            global $APPLICATION;
            $APPLICATION->ThrowException($this->getMessage('INSTALL_MIN_MAIN_VERSION_ERROR', ['#VERSION#' => $module]));
        }
    }

    public function DoUninstall()
    {
        $this->Uninstall();
    }

    /**
     * InstallDB
     *
     * @return bool|void
     */
    public function InstallDB()
    {
        self::UnInstallDB();
        foreach (self::TABLE as $table) {
            Modules\Utility::createDbTable($table);
        }
    }

    /**
     *
     */
    public function UnInstallDB()
    {
        foreach (self::TABLE as $table) {
            try {
                Modules\Utility::dropTable($table);
            } catch (Exception $e) {
            }
        }
    }

    /*
     * InstallEvent
     */

    public function InstallEvent()
    {
        foreach (self::EVENT as $event) {
            $this->registerEventHandler($event['modul'], $event['event'], $event['class'], $event['func']);
        }
    }

    public function UnInstallEvent()
    {
        foreach (self::EVENT as $event) {
            $this->unRegisterEventHandler($event['modul'], $event['event'], $event['class'], $event['func']);
        }
    }

    public function InstallAgent()
    {
        foreach (self::AGENT as $agent) {
            self::AddAgent(
                $agent['name'],
                $agent['period'],
                $agent['interval'],
            );
        }
    }

    public function UnInstallAgent()
    {
        foreach (self::AGENT as $agent) {
            CAgent::RemoveAgent($agent['name'], $this->MODULE_ID);
        }
    }

    public function InstallFiles($arParams = array()) {
        foreach (self::FILES as $key=>$value) {
            CopyDirFiles(__DIR__ . $key, Application::getDocumentRoot() . $value, true, true);
        }
    }

    public function UnInstallFiles() {
        foreach (self::FILES as $key=>$value) {
            DeleteDirFiles(__DIR__ . $key, Application::getDocumentRoot() . $value);
        }
    }

}
